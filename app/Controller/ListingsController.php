<?php
App::uses('AppController', 'Controller');
/**
 * Listings Controller
 *
 * @property Listing $Listing
 */
class ListingsController extends AppController {
	public $presetVars = true; //search configured using model
	public $paginate = array();
	
	public function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow('index','view');
		//update the vanity name for the url
		$this->set('title_for_layout', 'Events');
	}

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Listing->recursive = 0;
		$this->Prg->commonProcess();//setup Search
		$this->paginate['conditions'] = $this->Listing->parseCriteria($this->passedArgs);
		$this->set('listings', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->Listing->id = $id;
		if (!$this->Listing->exists()) {
			throw new NotFoundException(__('Invalid listing'));
		}
		$this->set('listing', $this->Listing->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Listing->create();
			$this->request->data['Listing']['user_id'] = $this->Auth->user('id');
			if ($this->Listing->save($this->request->data)) {
				$this->Session->setFlash(__('The listing has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The listing could not be saved. Please, try again.'));
			}
		}
		$users = $this->Listing->User->find('list');
		$this->set(compact('users'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->Listing->id = $id;
		if (!$this->Listing->exists()) {
			throw new NotFoundException(__('Invalid listing'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Listing->save($this->request->data)) {
				$this->Session->setFlash(__('The listing has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The listing could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->Listing->read(null, $id);
		}
		$users = $this->Listing->User->find('list');
		$this->set(compact('users'));
	}

/**
 * delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
/* Only admins can delete events
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Listing->id = $id;
		if (!$this->Listing->exists()) {
			throw new NotFoundException(__('Invalid listing'));
		}
		if ($this->Listing->delete()) {
			$this->Session->setFlash(__('Listing deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Listing was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
*/
/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Listing->recursive = 0;
		$this->Prg->commonProcess();//setup Search
		$this->paginate['conditions'] = $this->Listing->parseCriteria($this->passedArgs);
		$this->set('listings', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		$this->Listing->id = $id;
		if (!$this->Listing->exists()) {
			throw new NotFoundException(__('Invalid listing'));
		}
		$this->set('listing', $this->Listing->read(null, $id));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Listing->create();
			if ($this->Listing->save($this->request->data)) {
				$this->Session->setFlash(__('The listing has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The listing could not be saved. Please, try again.'));
			}
		}
		$users = $this->Listing->User->find('list');
		$this->set(compact('users'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		$this->Listing->id = $id;
		if (!$this->Listing->exists()) {
			throw new NotFoundException(__('Invalid listing'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Listing->save($this->request->data)) {
				$this->Session->setFlash(__('The listing has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The listing could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->Listing->read(null, $id);
		}
		$users = $this->Listing->User->find('list');
		$this->set(compact('users'));
	}

/**
 * admin_delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Listing->id = $id;
		if (!$this->Listing->exists()) {
			throw new NotFoundException(__('Invalid listing'));
		}
		if ($this->Listing->delete()) {
			$this->Session->setFlash(__('Listing deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Listing was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
