<div id="header">
			<nav>
				<ul>
					<li>
						<h4><?php echo $this -> Html -> link('Home', '/'); ?></h4>
					</li>
					<li>
						<h4><?php echo $this -> Html -> link('Events', '/listings/'); ?></h4>
					</li>
					<li>
						<h4><?php echo $this -> Html -> link('Items', '/items/'); ?></h4>
					</li>
					<li>
						<h4><?php echo ($request['isLoggedIn'] ? $this->Html->link('Log out', array('admin'	=> false, 'controller' => 'users', 'action' => 'logout')) : $this->Html->link('Log in', array('admin' => false,'controller' => 'users', 'action' => 'login')))?></h4>
					</li>
				</ul>
			</nav>
			<div class="search_callout">
				<h1>Search: </h1>
				<span id="search_listings" onclick="toggle_search('listings_search');">Events</span><span id="search_items" onclick="toggle_search('items_search');">Items</span>
			</div>
			<div class="search_container" style="background: white;">
				<div class="listings search" id="listings_search">
					<h3>Search Events</h3>
					<?php 
						echo $this->Form->create('Listing',array(
							'url' => array_merge(array('controller' => 'listings', 'action' => 'index'), $this->params['pass']),
							'novalidate' => true
						));
						echo $this->Form->input('id', array('type' => 'text'));
						echo $this->Form->input('title');
						echo $this->Form->input('subject', array(
							'empty' => 'Choose One',
							'options' => $subjects
						));
						echo $this->Form->input('description');
						echo $this->Form->input('creator');
						echo $this->Form->input('publisher');
						echo $this->Form->input('contributor');
						echo $this->Form->input('lang');
						echo $this->Form->input('rights');
						//TODO: make date search work  
						echo $this->Form->input('date', array('empty' => true));
						echo $this->Form->input('Listing.Keyword', array('multiple' => 'multiple'));
						
						echo $this->Form->end('Search')
					?>
				</div>
				<div class="items search" id="items_search">
					<h3>Search Items</h3>
					<?php 
						echo $this->Form->create('Item',array(
							'url' => array_merge(array('controller' => 'items', 'action' => 'index'), $this->params['pass']),
							'novalidate' => true
						));
						echo $this->Form->input('id', array('type' => 'text'));
						echo $this->Form->input('author');
						echo $this->Form->input('description');
						//TODO: make Item Keyword search work echo $this->Form->input('Item.Keyword', array('multiple' => 'multiple'));
						echo $this->Form->end('Search')
					?>
				</div>
			</div>
		</div>
		
<script type="text/javascript" charset="utf-8">
		function toggle_search(cur_item){
			var listings_search = document.getElementById("listings_search");
			var items_search = document.getElementById("items_search");
			
			if(cur_item == 'listings_search'){
				cur_item = listings_search;
				other_item = items_search;
			}
			
			if(cur_item == 'items_search'){
				cur_item = items_search;
				other_item = listings_search;
			}
			
			console.log("Display none: " + (cur_item.style.display == "none"));
			
			if(cur_item.style.display != "none"){
				cur_item.style.display = "none";
			} else {
				cur_item.style.display = "block";
			}
			other_item.style.display = "none";
		}
		
		window.onload = function(){
			var listings_search = document.getElementById("listings_search");
			var items_search = document.getElementById("items_search");
			
			listings_search.style.display = "none";
			items_search.style.display = "none";
		}
	</script>