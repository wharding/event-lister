<div class="sidebar">
	<div class="actions">
		<h3><?php echo __('Actions'); ?></h3>
		<ul>
			<li><?php echo $this -> Html -> link(__('List Events'), '/listings/'); ?> </li>
			<li><?php echo $this -> Html -> link(__('List Items'), '/items/'); ?> </li>
			<?php if($request['isLoggedIn']): ?>
				<h6>User Actions</h6>
				<ul>
					<li><?php echo $this -> Html -> link(__('New Event'), '/listings/add/'); ?></li>
					<!-- <li><?php echo $this -> Html -> link(__('New Item'), array('controller' => 'items', 'action' => 'add')); ?> </li> -->
				</ul>
			<?php endif; ?>
			<?php if($user_info['role'] === 'admin'): ?>
			<li>
				<h6>Admin Actions</h6>
				<ul>
					<li><?php echo $this -> Html -> link(__('Manage Events'), array('admin' => 'true', 'controller' => 'listings', 'action' => 'index')); ?> </li>
					<li><?php echo $this -> Html -> link(__('Manage Items'), array('admin' => 'true', 'controller' => 'items', 'action' => 'index')); ?> </li>
					<li><?php echo $this -> Html -> link(__('Manage Users'), array('admin' => 'true', 'controller' => 'users', 'action' => 'index')); ?> </li>
					<li><?php echo $this -> Html -> link(__('Manage Keywords'), array('admin' => 'true', 'controller' => 'keywords', 'action' => 'index')); ?> </li>	
					<li><?php echo $this -> Html -> link(__('New User'), array('admin' => 'true', 'controller' => 'users', 'action' => 'add')); ?> </li>
					<li><?php echo $this -> Html -> link(__('New Item'), array('admin' => 'true', 'controller' => 'items', 'action' => 'add')); ?> </li>	
					<li><?php echo $this -> Html -> link(__('New Keyword'), array('admin' => 'true', 'controller' => 'keywords', 'action' => 'add')); ?> </li>	
				</ul>
			</li>				
			<?php endif; ?>
		</ul>
	</div>
</div>