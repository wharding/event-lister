<div class="items view">
<h2><?php  echo __('Item'); ?></h2>
	<dl>
		<dt><?php echo __('Event'); ?></dt>
		<dd>
			<?php echo $this->Html->link($item['Listing']['title'], array('controller' => 'listings', 'action' => 'view', $item['Listing']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Author'); ?></dt>
		<dd>
			<?php echo h($item['Item']['author']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Description'); ?></dt>
		<dd>
			<?php echo h($item['Item']['description']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('File'); ?></dt>
		<dd>
			<?php //we have to write the element our since CakePHP doesn't handle target attributes in images ?>
			<a href="<?php echo $this->Html->url($item['Item']['public_dir']) ?>" target="_blank"><?php echo $this->Html->image($item['Item']['public_dir_thumb'], array('width' => '100', 'title' => 'Click to view')); ?></a>
			&nbsp;
		</dd>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($item['Item']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($item['Item']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($item['Item']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="related">
	<h3><?php echo __('Related Keywords'); ?></h3>
	<?php if (!empty($item['Keyword'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Keyword'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($item['Keyword'] as $keyword): ?>
		<tr>
			<td><?php echo $keyword['value']; ?></td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

</div>
