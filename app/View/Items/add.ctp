<div class="items form">
<?php echo $this->Form->create('Item', array('type' => 'file')); ?>
	<fieldset>
		<legend><?php echo __('Add Item'); ?></legend>
	<?php
		echo $this->Form->input('description');
		echo $this->Form->input('author');
		echo $this->Form->input('file', array('type' => 'file', 'label' => 'File'));
		echo $this->Form->input('Item.Keyword', array('label' => 'Keywords', 'type' => 'select', 'multiple' => true));
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
