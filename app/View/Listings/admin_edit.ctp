<div class="listings form">
<?php echo $this->Form->create('Listing'); ?>
	<fieldset>
		<legend><?php echo __('Admin Edit Event'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('title');
		echo $this->Form->input('subject', array(
			'options' => array(
				'Culture' => 'Culture',
				'Music' => 'Music',
				'Society' => 'Society',
				'Contest' => 'Contest',
				'Festival' => 'Festival',
				'About' => 'About'
			)
		));
		echo $this->Form->input('description');
		echo $this->Form->input('creator');
		echo $this->Form->input('publisher');
		echo $this->Form->input('contributor');
		echo $this->Form->input('lang');
		echo $this->Form->input('rights');
		echo $this->Form->input('lts', array('label' => 'Long Term Storage'));
		echo $this->Form->input('date');
		echo $this->Form->input('user_id');
		echo $this->Form->input('Keyword');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
