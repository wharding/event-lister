<div class="listings index">
	<h2><?php echo __('Listings'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('title'); ?></th>
			<th><?php echo $this->Paginator->sort('subject'); ?></th>
			<th><?php echo $this->Paginator->sort('description'); ?></th>
			<th><?php echo $this->Paginator->sort('creator'); ?></th>
			<th><?php echo $this->Paginator->sort('publisher'); ?></th>
			<th><?php echo $this->Paginator->sort('contributor'); ?></th>
			<th><?php echo $this->Paginator->sort('lang'); ?></th>
			<th><?php echo $this->Paginator->sort('rights'); ?></th>
			<th><?php echo $this->Paginator->sort('lts'); ?></th>
			<th><?php echo $this->Paginator->sort('date'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th><?php echo $this->Paginator->sort('user_id'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
	foreach ($listings as $listing): ?>
	<tr>
		<td><?php echo h($listing['Listing']['id']); ?>&nbsp;</td>
		<td><?php echo h($listing['Listing']['title']); ?>&nbsp;</td>
		<td><?php echo h($listing['Listing']['subject']); ?>&nbsp;</td>
		<td><?php echo h($listing['Listing']['description']); ?>&nbsp;</td>
		<td><?php echo h($listing['Listing']['creator']); ?>&nbsp;</td>
		<td><?php echo h($listing['Listing']['publisher']); ?>&nbsp;</td>
		<td><?php echo h($listing['Listing']['contributor']); ?>&nbsp;</td>
		<td><?php echo h($listing['Listing']['lang']); ?>&nbsp;</td>
		<td><?php echo h($listing['Listing']['rights']); ?>&nbsp;</td>
		<td><?php echo h($listing["Listing"]['lts'] ? 'Yes' : 'No'); ?>&nbsp;</td>
		<td><?php echo h($listing['Listing']['date']); ?>&nbsp;</td>
		<td><?php echo h($listing['Listing']['created']); ?>&nbsp;</td>
		<td><?php echo h($listing['Listing']['modified']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($listing['User']['name'], array('controller' => 'users', 'action' => 'view', $listing['User']['id'])); ?>
		</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $listing['Listing']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $listing['Listing']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $listing['Listing']['id']), null, __('Are you sure you want to delete # %s?', $listing['Listing']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>

	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
