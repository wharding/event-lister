<?php
App::uses('AppModel', 'Model');
/**
 * Keyword Model
 *
 * @property Item $Item
 * @property Listing $Listing
 */
class Keyword extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'value';
	
/**
 * Search Filters
 *
 * @var array
 */
 	public $filterArgs = array(
		'value' => array('type' => 'like', 'allowEmpty' => true)
	);

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'value' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Please enter a keyword',
				'required' => true,
			),
			'maxlength' => array(
				'rule' => array('maxlength', 200),
				'message' => 'The enterd keywork was too long, please use 200 characters or less',
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasAndBelongsToMany associations
 *
 * @var array
 */
	public $hasAndBelongsToMany = array(
		'Item' => array(
			'className' => 'Item',
			'joinTable' => 'items_keywords',
			'foreignKey' => 'keyword_id',
			'associationForeignKey' => 'item_id',
			'unique' => 'keepExisting',
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		),
		'Listing' => array(
			'className' => 'Listing',
			'joinTable' => 'keywords_listings',
			'foreignKey' => 'keyword_id',
			'associationForeignKey' => 'listing_id',
			'unique' => 'keepExisting',
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		)
	);

}
