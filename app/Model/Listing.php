<?php
App::uses('AppModel', 'Model');
/**
 * listing Model
 *
 * @property User $User
 * @property Item $Item
 * @property Keyword $Keyword
 */
class Listing extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'title';
	
	public $actsAs = array(
		'Search.Searchable',//allow searching via the search plugin
	);
	
	//config for search plugin
	public $filterArgs = array(
		'id' => array('type' => 'value', 'allowEmpty' => true, 'empty' => true/*hide from url if empty*/),
		'title' => array('type' => 'like', 'allowEmpty' => true, 'empty' => true/*hide from url if empty*/),
		'subject' => array('type' => 'value', 'allowEmpty' => true, 'empty' => true/*hide from url if empty*/),
		'description' => array('type' => 'like', 'allowEmpty' => true, 'empty' => true/*hide from url if empty*/),
		'creator' => array('type' => 'like', 'allowEmpty' => true, 'empty' => true/*hide from url if empty*/),
		'publisher' => array('type' => 'like', 'allowEmpty' => true, 'empty' => true/*hide from url if empty*/),
		'contributor' => array('type' => 'like', 'allowEmpty' => true, 'empty' => true/*hide from url if empty*/),
		'lang' => array('type' => 'value', 'allowEmpty' => true, 'empty' => true/*hide from url if empty*/),
		'rights' => array('type' => 'like', 'allowEmpty' => true, 'empty' => true/*hide from url if empty*/),
		//TODO implement date search
		'date' => array('type' => 'subquery', 'allowEmpty' => true, 'empty' => true, 'method' => 'searchByDate', 'field' => 'Listing.id'),
		'Listing.Keyword' => array('type' => 'subquery', 'allowEmpty' => true, 'empty' => true, 'method' => 'searchByKeywords', 'field' => 'Listing.id')
	);

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'title' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Please enter a title',
				'allowEmpty' => false,
				'required' => true
			),
			'maxlength' => array(
				'rule' => array('maxlength', 50),
				'message' => 'You title is too long, please use 50 characters or less'
			),
		),
		'subject' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Please enter a valid subject',
				'allowEmpty' => false,
				'required' => true
			),
			'inList' => array(
				'rule' => array('inList', array(
					'Culture',
					'Music',
					'Society',
					'Contest',
					'Festival',
					'About'
				)),
				'message' => 'The provided subject was not valid'
			)
		),
		'description' => array(
			'maxlength' => array(
				'rule' => array('maxlength', 200),
				'message' => 'The description is too long, please use 200 characters or less',
				'required' => false,
				'allowEmpty' => true
			),
		),
		'creator' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Please enter a creator',
				'required' => true,
				'allowEmpty' => false
			),
			'maxlength' => array(
				'rule' => array('maxlength', 50),
				'message' => 'the provided creator is too long, please use 50 characters or less'
			),
		),
		'publisher' => array(
			'maxlength' => array(
				'rule' => array('maxlength', 50),
				'message' => 'The provided publisher is too long, please use 50 characters or less',
				'required' => false,
				'allowEmpty' => true
			),
		),
		'contributor' => array(
			'maxlength' => array(
				'rule' => array('maxlength', 50),
				'message' => 'The provided contributor is too long, please use 50 characters or less',
				'required' => false,
				'allowEmpty' => true
			),
		),
		'lang' => array(
			'maxlength' => array(
				'rule' => array('maxlength', 2),
				'message' => 'Language must be two characters',
				'required' => false,
				'allowEmpty' => true
			),
			'minlength' => array(
				'rule' => array('minlength', 2),
				'message' => 'Language must be two characters'
			),
		),
		'rights' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Please enter rights',
				'required' => true,
				'allowEmpty' => false
			),
			'maxlength' => array(
				'rule' => array('maxlength', 200),
				'message' => 'The provided right are too long, please use 200 characters or less'
			),
		),
		'lts' => array(
			'boolean' => array(
				'rule' => array('boolean'),
				'required' => false,
				'allowEmpty' => true
			),
		),
		'date' => array(
			'date' => array(
				'rule' => array('date'),
				'message' => 'Please enter a date',
				'required' => true,
				'allowEmpty' => false
			),
		),
		'user_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				'required' => true,
				'allowEmpty' => false
			)
		),
		'Keyword' => array(
			'multiple' => array(
				'rule' => array('multiple', array('max' => 5)),
				'message' => 'You can only select 5 keywords maximum',
				'required' => false,
				'allowEmpty' => true
			)
		)
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Item' => array(
			'className' => 'Item',
			'foreignKey' => 'listing_id',
			'dependent' => true,//true to delete items when the listing for them is deleted
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);


/**
 * hasAndBelongsToMany associations
 *
 * @var array
 */
	public $hasAndBelongsToMany = array(
		'Keyword' => array(
			'className' => 'Keyword',
			'joinTable' => 'keywords_listings',
			'foreignKey' => 'listing_id',
			'associationForeignKey' => 'keyword_id',
			'unique' => 'keepExisting',
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		)
	);
	
	public function afterFind($results, $primary = false){
		//add a field to all listings to determine is the current user is the owner
		foreach($results as &$result){
			if(isset($result['Listing']['user_id']) && isset($_SESSION['Auth']['User']['id'])) {
				$result['Listing']['isOwner'] = ($_SESSION['Auth']['User']['id'] == $result['Listing']['user_id']);
			} else {
				$result['Listing']['isOwner'] = false;
			}
		}
		unset($result);
		return $results;
	}
	
	public function beforeSave($options = array()){
		//transforms the data array to allow correct saving of the HABTM relationship after validation
		foreach (array_keys($this->hasAndBelongsToMany) as $model){
			if(isset($this->data[$this->name][$model])){
				$this->data[$model][$model] = $this->data[$this->name][$model];
				unset($this->data[$this->name][$model]);
			}
		}
		return true;
	}
	
	//performs search by Dates
	public function searchByDate($data = array()){
		if(!isset($data['date']) || empty($data['date'])){
			return;
		}
							
		//format the query correctly, we also check to make sure we are actually passed a number to prevent DB problems
		$formattedDate = (!empty($data['date']['year']) && is_numeric($data['date']['year']) ? $data['date']['year'] : '____');
		$formattedDate .= '-';
		$formattedDate .= (!empty($data['date']['month']) && is_numeric($data['date']['month']) ? $data['date']['month'] : '__');;
		$formattedDate .= '-';
		$formattedDate .= (!empty($data['date']['day']) && is_numeric($data['date']['day']) ? $data['date']['day'] : '__');;
		
		$query = $this->getQuery('all', array(
			'conditions' => array(
				'Listing.date LIKE' => $formattedDate
			),
			'fields' => array(
				'id'
			)
		));
		
        $this->log($query, 'debug');
		return $query;
	}
	
	//performs serach for Keywords
	public function searchByKeywords($data = array()) {
		if(!isset($data['Keyword']) || empty($data['Keyword'])){
			return;
		}
		
        $this->Keyword->Behaviors->attach('Search.Searchable');
        $query = $this->Keyword->getQuery('all', array(
            'conditions' => array('Keyword.id'  => $data['Keyword']),
            'fields' => array('id'),
            'contain' => array('Keyword')
        ));
        
        
        return $query;
    }
	
}
