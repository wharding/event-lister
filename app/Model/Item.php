<?php
App::uses('AppModel', 'Model');
/**
 * Item Model
 *
 * @property Listing $Listing
 * @property Keyword $Keyword
 */
class Item extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'description';
	
	public $actsAs = array(
		'Search.Searchable',//allow searching via the search plugin
		'Upload.Upload' => array( //configurations for upload plugin
			'file' => array(
				'fields' => array(
					'dir' => 'file_dir',
					'type' => 'file_type',
					'size' => 'file_size'
				),
				'pathMethod' => 'primaryKey', //default
                'deleteOnUpdate' => true,
                'thumbnails' => true,
				'thumbnailSizes' => array(
                    'thumb' => '80x80'
                ),
                'thumbnailMethod' => 'imagick',//KU severs have imagick installed
                'mediaThumbnailType' => 'jpg',
                'thumbnailType' => 'jpg',
                //we are just going to open everything up, instead of placing this aribtrary limitations on the software
				// 'mimetypes' => array(
					// 'image/jpeg',
					// 'image/pjpeg',
					// 'application/msword',
					// 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
					// 'application/pdf',
					// 'audio/vnd.wave',
					// 'image/tiff',
					// 'audio/mpeg'
// 					
				// ),
				// 'extensions' => array(
					// 'jpg',
					// 'doc',
					// 'docx',
					// 'pdf',
					// 'wav',
					// 'tif',
					// 'tiff',
					// 'mp3'
				// )
			)
		),
	);
	
	//config for search plugin
	public $filterArgs = array(
		'id' => array('type' => 'value', 'allowEmpty' => true, 'empty' => true/*hide from url if empty*/),
		'author' => array('type' => 'like', 'allowEmpty' => true, 'empty' => true/*hide from url if empty*/),
		'description' => array('type' => 'like', 'allowEmpty' => true, 'empty' => true/*hide from url if empty*/),
		'Item.Keyword' => array('type' => 'subquery', 'allowEmpty' => true, 'empty' => true, 'method' => 'searchByKeywords', 'field' => 'Item.id')
	);
	
	
/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'description' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Please enter a description',
				'required' => true,
			),
			'maxlength' => array(
				'rule' => array('maxlength', 200),
				'message' => 'The provided description was too long, please use 200 characters or less',
			),
		),
		'author' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Please enter an author',
				'required' => true
			),
			'maxlength' => array(
				'rule' => array('maxlength', 50),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'listing_id' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'No event specified',
				'required' => true
			),
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'Invalid event key'
			),
			/*
			* Not currenlty using the following rule
			'isOwnerOrAdmin' => array(
				'rule' => array('isOwnerOrAdmin'),
				'message' => 'You are not authorized to create items for the specified event'
			)*/
		),
		'file' => array(
			'fileSize' => array(
				'rule' => 'isUnderPhpSizeLimit',
				'message' => 'File is too large'
			),
			//since where not checking above, we don't need this check any more either
			// 'fileType' => array(
				// 'rule'	=> 'isValidMimeType',
				// 'message' => 'File type not supported'
			// ),
			// 'fileExtension' => array(
				// 'rule' => 'isValidExtension',
				// 'message' => 'File extension not supported'
			// ),
			'completedUpload' => array(
				'rule' => 'isCompletedUpload',
        		'message' => 'File was not successfully uploaded'
			),
			'fileSuccessfulyWritten' => array(
		        'rule' => 'isSuccessfulWrite',
		        'message' => 'File was unsuccessfully written to the server'
		    )
		),
		'Keyword' => array(
			'multiple' => array(
				'rule' => array('multiple', array('max' => 5)),
				'message' => 'You can only select 5 keywords maximum',
				'required' => false,
				'allowEmpty' => true
			)
		)
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Listing' => array(
			'className' => 'Listing',
			'foreignKey' => 'listing_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

/**
 * hasAndBelongsToMany associations
 *
 * @var array
 */
	public $hasAndBelongsToMany = array(
		'Keyword' => array(
			'className' => 'Keyword',
			'joinTable' => 'items_keywords',
			'foreignKey' => 'item_id',
			'associationForeignKey' => 'keyword_id',
			'unique' => 'keepExisting',
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		)
	);
	
	//validates if the current user is the owner or admin of a corresponding listing
	public function isOwnerOrAdmin($check){
		if($_SESSION['Auth']['User']['role'] === 'admin'){
			return true;
		}
		$this->Listing->recursive = -1;
		$parent_listing = $this->Listing->findById($check['listing_id']);
		if(isset($parent_listing) && isset($_SESSION['Auth']['User']['id']) && $parent_listing['Listing']['user_id'] === $_SESSION['Auth']['User']['id']){
			return true;
		}
		
		
		//default
		return false;		
	}

	public function afterFind($results, $primary = false){
		foreach($results as &$result){
			//add a file for the public directory to this image
			//TODO update to generic image for non-image files s
			$dir = ((isset($result['Item']['file_dir']) && $result['Item']['file_dir']) ? $result['Item']['file_dir'] . '/' : '');
			if(isset($result['Item']['file']) && (isset($result['Item']['file_type']) && preg_match('/^image\/|^application\/pdf$/',$result['Item']['file_type']))){
				$result['Item']['public_dir'] = '/files/item/file/' . $dir . $result['Item']['file'];
				$result['Item']['public_dir_thumb'] = '/files/item/file/' . $dir . '/thumb_' . preg_replace('/\.[a-zA-Z]+$/', '.jpg', $result['Item']['file'], 1);;
			} elseif(isset($result['Item']['file']) && (isset($result['Item']['file_type']))) { 
				$result['Item']['public_dir'] = '/files/item/file/' . $dir . $result['Item']['file'];
				$result['Item']['public_dir_thumb'] = '/img/thumb_no_preview.jpg';
			} else {
				$result['Item']['public_dir'] = '/img/no_preview.jpg';
				$result['Item']['public_dir_thumb'] = '/img/thumb_no_preview.jpg';
			}
			//set if this current user is the owner of this item
			if(isset($result['Item']['listing_id'])){
				$this->Listing->recursive = -1;
				$parent_listing = $this->Listing->findById($result["Item"]['listing_id']);
				if(isset($parent_listing) && isset($_SESSION['Auth']['User']['id']) && $parent_listing['Listing']['user_id'] === $_SESSION['Auth']['User']['id']){
					$result['Item']['isOwner'] = true;
				} else {
					$result['Item']['isOwner'] = false;
				}
			} else {
					$result['Item']['isOwner'] = false;
			}
		}
		unset($result);
		return $results;
	}
	
	//performs serach for Keywords 
	public function searchItemsByKeywords($data = array()) {
		if(!isset($data['Keyword'])){
			return;
		}
		
        $this->Keyword->Behaviors->attach('Search.Searchable');
        $query = $this->Keyword->getQuery('all', array(
            'conditions' => array('Keyword.id'  => $data['Keyword']),
            'fields' => array('id'),
            'contain' => array('Keyword')
        ));
        
        return $query;
    }
	
	public function beforeSave($options = array()){
		//transforms the data array to allow correct saving of the HABTM relationship after validation
		foreach (array_keys($this->hasAndBelongsToMany) as $model){
			if(isset($this->data[$this->name][$model])){
				$this->data[$model][$model] = $this->data[$this->name][$model];
				unset($this->data[$this->name][$model]);
			}
		}
		return true;
	}

		
}
