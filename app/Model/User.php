<?php
App::uses('AppModel', 'Model');
/**
 * user Model
 *
 * @property Listing $Listing
 */
class User extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'email' => array(
			'email' => array(
				'rule' => array('email'),
				'message' => 'Please enter a valid email',
				'required' => true,
				'allowEmpty' => false
			),
			'isUnique' => array(
				'rule' => 'isUnique',
				'message' => 'The provided email is already used'
			)
		),
		'password' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Please enter a password',
				'required' => true,
				'allowEmpty' => false
			)
		),
		'name' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Please enter a name',
				'required' => true,
				'allowEmpty' => false
			),
			'maxlength' => array(
				'rule' => array('maxlength', 50),
				'message' => 'The provided name was too long, please use 50 characters or less'
			),
		),
		'role' => array(
			'inList' => array(
				'rule' => array('inList', array(
					'user',
					'admin'
				)),
				'message' => 'The specified user role is invalid',
				'required' => true,
				'allowEmpty' => false
			)
		)
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Listing' => array(
			'className' => 'Listing',
			'foreignKey' => 'user_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);
	
	public function beforeSave($options = array()){
		/*Hash password before saving new user*/
	    if (isset($this->data[$this->alias]['password'])) {
			$this->data[$this->alias]['password'] = AuthComponent::password($this->data[$this->alias]['password']);
		}
		return true;
	} 

	public function beforeDelete($cascasde = true){
		$user = $this->find('first', array(
			'conditions' => array('User.id' => $this->id),
			'recursive' => -1
		));
		//ensure this is not the last admin user
		if($user['User']['role'] === 'admin'){
			$admin_count = $this->find('count', array(
				'conditions' => array('User.role' => 'admin'),
				'recursive' => -1
			));
			if($admin_count == 1){
				return false;
			}
			
		}
		
		//default
		return true;
	}

}
